# LLM
LLM is an attempt at writing some sort of dynamic language server-side
scripting language -- I never got round to the dynamic part ;-)

It directly uses fastcgi to support multiple web servers. I wouldn't use
this on a server though as fastcgi is known to be quite memory leaky!

# Dependencies

* fcgi c++ library
* spawn-fcgi
* fcgi
* nginx
* clang++/g++/...

# Setup

I'm using nginx with the following configuration:

    server {
        listen 3321;
        server_name localhost;
        index index.llm;

        location ~ \.llm$ {
            fastcgi_pass   127.0.0.1:8000;
            fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
            fastcgi_param  SERVER_SOFTWARE    nginx;
            fastcgi_param  QUERY_STRING       $query_string;
            fastcgi_param  REQUEST_METHOD     $request_method;
            fastcgi_param  CONTENT_TYPE       $content_type;
            fastcgi_param  CONTENT_LENGTH     $content_length;
            fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
            fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
            fastcgi_param  REQUEST_URI        $request_uri;
            fastcgi_param  DOCUMENT_URI       $document_uri;
            fastcgi_param  DOCUMENT_ROOT      $document_root;
            fastcgi_param  SERVER_PROTOCOL    $server_protocol;
            fastcgi_param  REMOTE_ADDR        $remote_addr;
            fastcgi_param  REMOTE_PORT        $remote_port;
            fastcgi_param  SERVER_ADDR        $server_addr;
            fastcgi_param  SERVER_PORT        $server_port;
            fastcgi_param  SERVER_NAME        $server_name;
        }
    }
