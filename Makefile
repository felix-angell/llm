cxx := clang++
cxx_source = $(wildcard src/*cpp)
cxx_flags = -Wall -std=c++14 -Iinclude/ -g -lfcgi -lfcgi++
out_dir := bin
out_name := llm

all: $(cxx_source)
	@mkdir -p $(out_dir)/
	$(cxx) $(cxx_flags) $(cxx_source) -o $(out_dir)/$(out_name)

spawn:
	spawn-fcgi -p 8000 -n $(out_dir)/$(out_name)
