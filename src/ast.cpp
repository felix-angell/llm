#include "ast.hpp"

void ElementLiteral::codegen() {
    std::cout << (content);
}

void ElementDecl::codegen() {
    std::cout << ("<" + tag);

    if (attributes->size() > 0) {
        std::cout << (" "); // space
    }

    for (auto &attrib : *attributes) {
        attrib->codegen();
    }

    std::cout << (">");

    for (auto &child : *children) {
        child->codegen();
    }

    std::cout << ("</" + tag + ">");
}

void ElementAttribute::codegen() {
    std::cout << (attribute + "=\"");
    child->codegen();
    std::cout << ("\"");
}
