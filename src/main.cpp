#include <iostream>
#include <fcgio.h>

#include "lexer.hpp"
#include "parser.hpp"

int main() {
    // Backup the stdio streambufs
    std::streambuf* cin_streambuf = std::cin.rdbuf();
    std::streambuf* cout_streambuf = std::cout.rdbuf();
    std::streambuf* cerr_streambuf = std::cerr.rdbuf();

    FCGX_Request request;

    FCGX_Init();
    FCGX_InitRequest(&request, 0, 0);

    while (!FCGX_Accept_r(&request)) {
        fcgi_streambuf cin_fcgi_streambuf(request.in);
        fcgi_streambuf cout_fcgi_streambuf(request.out);
        fcgi_streambuf cerr_fcgi_streambuf(request.err);
	
	    const char* filename = FCGX_GetParam("SCRIPT_FILENAME", request.envp);

        File file = File(filename);

        std::cin.rdbuf(&cin_fcgi_streambuf);
        std::cout.rdbuf(&cout_fcgi_streambuf);
        std::cerr.rdbuf(&cerr_fcgi_streambuf);

        std::cout << "Content-type: text/html\r\n\r\n";

        Lexer lexer;
        lexer.lex(&file);

        Parser parser;
        parser.parse(&file);
    }

    std::cin.rdbuf(cin_streambuf);
    std::cout.rdbuf(cout_streambuf);
    std::cerr.rdbuf(cerr_streambuf);

    return 0;
}