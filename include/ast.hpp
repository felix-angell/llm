#ifndef _LLM_AST_HPP
#define _LLM_AST_HPP

#include <iostream>
#include <vector>

class Node {
public:
    virtual void codegen() = 0;    
};

enum PrimitiveType {
    TYPE_INT,
    TYPE_DOUBLE,
    TYPE_STRING,
    TYPE_CHAR,
};

class Type {
public:
    PrimitiveType type;

    Type(PrimitiveType type) {
        this->type = type;
    }
};

class HTML : public Node {
public:
    HTML() {}
    virtual ~HTML() {}
    virtual void codegen() = 0;    
};

enum ElementLiteralType {
    LIT_NUMBER,
    LIT_STRING,
    LIT_CHAR,
    LIT_UNKNOWN,
}; 

class ElementLiteral : public HTML {
public:
    ElementLiteralType type;
    std::string content;

    ElementLiteral(ElementLiteralType type, std::string content) {
        this->type = type;
        this->content = content;
    }

    virtual void codegen();
};

class ElementAttribute {
public:
    std::string attribute;
    ElementLiteral *child;

    ElementAttribute(std::string attribute, ElementLiteral *child) {
        this->attribute = attribute;
        this->child = child;
    }

    virtual ~ElementAttribute() {
        delete child;
    }

    virtual void codegen();
};

class ElementDecl : public HTML {
public:
    std::string tag;
    std::vector<HTML*> *children; 
    std::vector<ElementAttribute*> *attributes;

    ElementDecl(std::string tag) {
        this->tag = tag;
        this->children = new std::vector<HTML*>;
        this->attributes = new std::vector<ElementAttribute*>;
    }

    void appendChild(HTML *child) {
        this->children->push_back(child);
    }

    void appendAttrib(ElementAttribute *attrib) {
        this->attributes->push_back(attrib);
    }

    virtual ~ElementDecl() {
        this->children->clear();
        this->attributes->clear();
    }

    virtual void codegen();    
};

class Expr {
public:
    Expr() {}
    virtual ~Expr();
    virtual void codegen() = 0;    
};

#endif // _LLM_AST_HPP