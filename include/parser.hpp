#ifndef _LLM_PARSER_HPP
#define _LLM_PARSER_HPP

#include <vector>
#include <map>

#include "file.hpp"
#include "ast.hpp"

class Parser {
private:
    const std::string FUNC_KEYWORD = "fn";
    std::map<std::string, PrimitiveType> types;

public:
    int pos;
    int streamSize;
    File *file;

    Parser() {
        types["int"] = TYPE_INT;
        types["double"] = TYPE_DOUBLE;
        types["string"] = TYPE_STRING;
        types["rune"] = TYPE_CHAR;
    }

    bool matchToken(std::string content, TokenType type, int ahead);
    bool isType(std::string what);

    Node *pushNode(Node *n);

    Type *parseType();
    Node *parseNode();
    ElementLiteral *parseElementLiteral();
    ElementDecl *parseElementDecl();
    ElementAttribute *parseElementAttribute();
    HTML *parseHTML();

    Token *consumeToken();
    Token *peek(int ahead = 0);

    void parse(File* file);
    void startParsingFiles(std::vector<File> *files);

    virtual ~Parser() {}
};

#endif // _LLM_PARSER_HPP