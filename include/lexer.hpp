#ifndef _LLM_LEXER_HPP
#define _LLM_LEXER_HPP

#include <iostream>
#include <vector>

#include "file.hpp"

class Lexer {
private:
    File *file;
    std::string buffer;

    int pos;
    int inputLength;
    int lineNumber;
    int charNumber;
    char currentChar;
    bool running;

public:
	Lexer() {}

    std::string flushBuffer();
    char consumeCharacter();

    void recognizeNumber();
    void recognizeOperator();
    void recognizeSeparator();
    void recognizeIdentifier();
    void recognizeCharacter();
    void recognizeString();
    
    void getNextToken();
    void skipCharacter();

    void lex(File *file);

    void pushToken(TokenType type);
    void eatComments();

    virtual ~Lexer() {}
};

#endif // _LLM_LEXER_HPP